# LTS specific rules

Through its [Debian LTS and Debian
ELTS](https://www.freexian.com/services/debian-lts.html) service offering,
Freexian is funding CVE triage and security updates for the benefits of
its customers and the Debian community.

While this is a big achievement, we believe that we need to do more than
this to ensure the long term viability of the Debian LTS project. In
particular, we believe that we need to invest in infrastructure:

* we want to improve existing infrastructure that we rely on, in
  particular when our usage has created a burden for other Debian teams in
  charge of those infrastructure
* we want to create new infrastructure to enable new and better workflows

With the agreement of all paid LTS members, we are putting aside a part of
the LTS budget to be used in funding various projects.

# Who can submit projects?

Anyone can submit project proposals as long as they can explain how it
benefits Debian LTS (even if only in the long term). We welcome, in
particular, project proposals from Debian core teams that provide
infrastructure that the Debian LTS project relies on.

# How will project proposals be approved?

Ultimately, Raphaël Hertzog (as Freexian's owner) selects which projects
will be funded. But paid Debian LTS contributors will vote upon open
project proposals and it is expected that the result will be respected.

Depending on the outcome, a project proposal can be either:

* accepted: the proposal will be funded
* refused: the proposal is dropped
* deferred: the proposal is kept for a future vote (this is expected for good proposals when there's not enough funding to accept them all)

# Who can make bids?

If the project submitter does not want to implement the project
themselves, then anyone can make bids for the project.

# How will the winning bid be selected?

Selecting a winning bid is a highly subjective decision. One must put
into balance the experience of the executor, its relationship with Debian
and the affected teams, the expected cost, the desired quality attributes,
etc.

Freexian will thus delegate this decision to one specific person for each
project. Ideally that person is appointed when the request for bid is
opened and takes the Reviewer role too.

It is expected that the process somehow favors members of the
teams in charge of the infrastructure/software involved in the project.
Failing that, paid Debian LTS contributors would be preferred.

# Who will review the work?

If the project proposal does not define any reviewer, then Freexian will
appoint one of the paid LTS contributors as reviewer.
