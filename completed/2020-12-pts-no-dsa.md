# Project Data

| Title | Better no-dsa support in PTS |
| ----- | ----- |
| **Funding source** | LTS |
| **Submittter** | Debian Security Team <security@debian.org> |
| **Executor** | Carles Pina i Estany <carles@pina.cat> |
| **Reviewer** | Sébastien Delafond <seb@debian.org> |
| **Proposal date** | 2020-12-04 |
| **Approval date** | 2020-12-11 |
| **Request for bids** | https://salsa.debian.org/freexian-team/project-funding/-/issues/2 |
| **Project management issue** | https://salsa.debian.org/freexian-team/project-funding/-/issues/4 |
| **Completion date** | 2021-02-26 |

----------------------------------------

# Rationale

Provide better state tracking for security issues not warranting a DSA
to reduce toil in communication and provide a better overview to
maintainers.

# Description

Provide a representation/coordination of <no-dsa> security issues in the
PTS.

## Project Details

If a package has issues in `<no-dsa>` status, the "new" PTS currently
displays it with "N ignored security issues in buster" (see for
instance. src:nss), which is misleading: the old-style PTS correctly
showed them as "There are 5 open security issues, please fix them".

These issues should instead be shown as actionable: `<no-dsa>` does not
mean that the security issue in ignored, it only means that it doesn't
warrant an out-of-band update via security.debian.org. Plenty of those
updates are fixed via point releases instead.

Syntax-wise in the Security Tracker there are three relevant states to
consider:

* `[buster] - foo <no-dsa>`
  Triaged as not warranting a DSA, requires further priorisation.

* `[buster] - foo <postponed>`
  Considered for a fix in a forthcoming stable update (either via a
  point update or piggybacked on a future DSA). Sometimes that means
  that a patch has been merged to a buster branch in git.

* `[buster] - foo <ignored>`
  Not considered for an update in stable. Possible reasons is that an
  update is too intrusive to backport to warrant the fix, that an update
  would introduce behavioral changes which do not warrant the fix, or
  that an issue is too harmless to be considered for an update.

These are also documented
[here](https://security-team.debian.org/security_tracker.html#issues-not-warranting-a-security-advisory).

From a maintainer's point of view, the actionable to be presented by the
PTS for an issue tagged as `<no-dsa>` is to triage it as <ignored>,
<postponed> or fix it via an s-p-u upload.

Right now we have some maintainers which check on these proactively and
in the (distant) past there was someone who coordinated these (Jonathan
Wiltshire), but this doesn't really scale very well and generates quite
a bit of toil. Some issues get communicated by the Security Team
(e.g. in bug mail) and some maintainers are reaching out proactively,
but it would be far better to handle this in the "new" PTS (the
old-style PTS can be ignored here).

## Completion Criteria

* for any issue listed as `<no-dsa>` show a landing page with some
  context of the CVE IDs (with a link to the Security Tracker) and
  explain the three options listed above. People can either send a mail
  to team@security.debian.org or commit to the Security Tracker repo.

* the PTS should also exclude CVE IDs listed in
  data/next-point-update.txt (or possibly review better options to track
  these).

## Estimated Effort

This will involve modifications to the internals of the PTS, and also
some changes related to the Security Tracker, but two person days / 16
hours sounds like a reasonable approximation.
