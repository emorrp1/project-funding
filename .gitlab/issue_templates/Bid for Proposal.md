Instructions: This template is intended as a guide for creating a bid for
proposal in response to a request for bids issue.

Replace any placeholders (text enclosed in parentheses) below as you complete
each section of the issue.  Be sure to remove this header section as well.

---

# Bid for Proposal

## Request for Bid Reference

(Provide a link to the Request for Bid that this issue responds to.)

## Project Task Summary

(In the form of a bulleted list, summarise the tasks which are required to
complete the project.)

## Project Task Detail

(Provide a detailed explanation for each task from the previous section.  The
amount of detail should be sufficient for someone evaluating the bid to validate
that you have a full understanding of the work to be done.  If the project has
an hourly labor component, this section should include an estimate of the hours
involved for each task.)

## Project Cost Information

(This section should include the estimated cost of the project.  The format
varies depending on whether the project is a single fixed cost, fixed cost per
milestone, hourly labor, or some other arrangement.  If you prefer to not
provide cost information in a public issue, then you can either make this issue
private or leave the issue public send the cost component by email to
raphael@freexian.com and the bid reviewer.  If you choose the latter
option, include a statement in this section noting that.)

## Qualification and Experience

(Put in this section anything that can convince your audience that you are
the right person for this task. Talk about any former experience with
similar projects. Share links to your contributions to free software
projects involving the same technologies/programming languages, etc.)

## Availability and Timeline

(This section should give some information about when you are available
and how much time you can spend on the project, and try to set some
reasonable expectations in terms of duration of the project once you have
started to work on it.)

## Debian Community Impact

(If you are offering to implement a project for a Debian team where you
are involved, we want to make sure that funding of this project will not
cause any trouble to the team. Please confirm that this project was
discussed within the team and summarize the discussion and its outcome)

/label ~"Bid for proposal"
